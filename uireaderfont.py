from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication


class Main:
    def __init__(self):
        self.mainui = loadUi('formfont.ui')
        self.mainui.show()
        
        self.mainui.fcmbFontStyle.currentFontChanged.connect(self.changefont)
        self.mainui.cmbFontSize.currentIndexChanged.connect(self.changefonsize)
        
    def changefont(self, font):
        cursor = self.mainui.textEditMainText.textCursor()
        
        if not cursor.hasSelection():
            return
        
        cursor.beginEditBlock()
        fontformat = cursor.charFormat()
        fontformat.setFont(font)
        cursor.setCharFormat(fontformat)
        cursor.endEditBlock()
        
    def changefonsize(self):
        cursor = self.mainui.textEditMainText.textCursor()
        if not cursor.hasSelection():
            return
        
        fontsize = int(self.mainui.cmbFontSize.currentText())
        cursor.beginEditBlock()
        fontformat = cursor.charFormat()
        fontformat.setFontPointSize(fontsize)
        cursor.setCharFormat(fontformat)
        cursor.endEditBlock()


if __name__ == '__main__':
    app = QApplication([])
    main = Main()
    app.exec()